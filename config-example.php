<?php
    // SPDX-License-Identifier: CC0-1.0
    
    //RINOMINATE IL FILE IN config.php!

    $db_host = "aaa";
    $db_port = "3306";
    $db_user = "bbb";
    $db_password = "ccc";
    $db_name = "dddd";
    $key = "passwordRSS";

    $threshold_approved_proposal = 0.8;
    $threshold_discarded_proposal = -0.5;
    $quorum_approval_proposal = 2;
    $threshold_approved_summary = 1;
    $system_user_id = 3; // SELECT id FROM users_am WHERE username = 'sistema';
    $expireTimeoutDays = 30;

    $db = null;
    try {
        $db=new PDO("mysql:host={$db_host};port={$db_port};dbname={$db_name}",$db_user,$db_password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOEXCEPTION $e) {
        print($e->getMessage());
    }
    $includeOK = true; // Do not change that!
?>
