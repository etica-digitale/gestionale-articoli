<?php if (!$includeOK) die() ?>
<?php if ($status == "proposto" || $status == "approvato" || $status == "in-lavorazione"):?>
    <div>
        <h2>Approvazione proposta</h2>
        <form method="POST">
            <input type="hidden" id="type" name="type" value="approvazione-proposta">

            <label for="value">Valore:</label>
            <select id="value" name="value" value=1>
                <option value=-2>Fortemente contrario</option>
                <option value=-1>Contrario</option>
                <option value=0>Neutrale</option>
                <option value=1 selected>Favorevole</option>
                <option value=2>Super favorevole</option>
            </select> 

            <label for="comment"><b>Commento</b></label>
            <textarea name="comment"></textarea>

            <button type="submit">Fatto!</button>
        </form>
    </div>
<?php endif ?>
