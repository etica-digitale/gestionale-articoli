<?php if (!$includeOK) die() ?>
<div id="articles-list">
    <h1> Lista articoli </h1>
    <?php 
        $sql = '
            SELECT 
                a.*,
                pu.username AS proposing_user,
                au.username AS assigned_user,
                le.text AS last_comment,
                es.text AS summary,
                count(esa.value) AS summary_approval_count,
                avg(epa.value) AS approval_proposal_avg,
                (eupa.id IS NULL AND a.status = "proposto") AS approval_not_done,
                count(epa.value) AS approval_proposal_count,
                (
                    (eupa.id IS NULL AND a.status = "proposto")
                    OR (eusa.id IS NULL AND a.status = "in-lavorazione")
                    OR (a.assignedTO = :id AND a.status = "in-lavorazione" AND es.text IS NULL) 
                ) AS require_attention       
            FROM articles_am a
            INNER JOIN events_am pe ON pe.article = a.id AND pe.type = "proposta"
            INNER JOIN users_am pu ON pu.id = pe.user
            LEFT JOIN users_am au ON au.id = a.assignedTO
            INNER JOIN events_am le ON le.article = a.id 
                AND NOT EXISTS (
                    SELECT * FROM events_am e2 
                    WHERE e2.article = a.id AND le.createdAt < e2.createdAt
                )
            LEFT JOIN events_am es ON es.article = a.id AND es.type = "riassunto"
                AND NOT EXISTS(
                    SELECT * FROM events_am e2
                    WHERE e2.article = es.article AND e2.type = "riassunto" AND e2.createdAt > es.createdAt
                )
            LEFT JOIN events_am esa ON esa.article = a.id AND esa.type = "approvazione-riassunto"
                AND NOT EXISTS (
                    SELECT * FROM events_am e2
                    WHERE e2.article = esa.article
                        AND e2.user = esa.user 
                        AND e2.type = "approvazione-riassunto"
                        AND e2.createdAt > esa.createdAt
                )
            LEFT JOIN events_am epa ON epa.article = a.id AND epa.type = "approvazione-proposta"
                AND NOT EXISTS (
                    SELECT * FROM events_am e2
                    WHERE e2.article = epa.article
                        AND e2.user = epa.user 
                        AND e2.type = "approvazione-proposta"
                        AND e2.createdAt > epa.createdAt
                )
            LEFT JOIN events_am eupa ON eupa.article = a.id AND eupa.user = :id AND eupa.type = "approvazione-proposta"
                AND NOT EXISTS (
                    SELECT * FROM events_am e2
                    WHERE e2.article = eupa.article
                        AND e2.user = eupa.user 
                        AND e2.type = "approvazione-proposta"
                        AND e2.createdAt > eupa.createdAt
                )
            LEFT JOIN events_am eusa ON eusa.article = a.id AND eusa.user = :id AND eusa.type = "approvazione-riassunto"
                AND NOT EXISTS (
                    SELECT * FROM events_am e2
                    WHERE e2.article = eusa.article
                        AND e2.user = eusa.user 
                        AND e2.type = eusa.type
                        AND e2.createdAt > eusa.createdAt
                )
            GROUP BY a.id
            ORDER BY a.createdAt DESC
        ';
        $sth = $db->prepare($sql);
        $sth->execute(array('id' => $_SESSION["id"])); 
    ?>
     <?php while ($row = $sth->fetch(PDO::FETCH_ASSOC)): ?>
        <div class="article article-<?php print($row["status"]) ?>">
            <div class="article-id">
                <strong>ID:</strong> 
                #<?php print($row["id"]) ?> 
                <a href="view-comments.php?articleID=<?php print($row["id"]); ?>">
                    <span class="attention">
                        <?php if ($row["require_attention"]) print("(!!!)"); ?>
                    </span>
                </a>
            </div>
            <div class="article-status"><strong>Stato:</strong> <?php print($row["status"]) ?></div>
            <div class="article-link"><a href="<?php print($row["link"]) ?>"><?php print($row["link"]) ?></a></div>    
            <div class="article-proposing-user"><strong>Proposto da:</strong> <?php print($row["proposing_user"]) ?></div>    
            <div class="article-created-at"><strong>Data di creazione:</strong> <?php print($row["createdAt"]) ?></div>    
            <div class="article-expire-date"><strong>Data di scadenza:</strong> <?php print($row["expireDate"]) ?></div>    
            <?php if (is_null($row["assigned_user"])): ?>
                <div class="article-assigned-user"><strong>Non assegnato</strong></div>    
            <?php else: ?>
                <div class="article-assigned-user"><strong>Assegnato a:</strong> <?php print($row["assigned_user"]) ?></div>    
            <?php endif ?>

            <div class="article-approval">
                <strong>Approvazioni:</strong> 
                Proposta (<?php print($row["approval_proposal_avg"] . " " . $row["approval_proposal_count"] . "/" . $usersCount )?>), 
                Riassunto (<?php print($row["summary_approval_count"])?>)
            </div>
            <?php if ($row["status"] != "scartato"): ?>
            <div class="article-last-comment-header"><strong>Ultimo commento</strong></div>
            <div class="article-last-summary-header"><strong>Ultimo riassunto</strong></div>
            <div class="article-last-comment"><?php print(nl2br($row["last_comment"])) ?></div>
            <div class="article-last-summary">
                <?php if (!is_null($row["summary"])) print(nl2br($row["summary"])) ?>
            </div>
            <?php endif ?>
            <div class="article-approve">
                <form method="POST">
                    <input type="hidden" id="action" name="action" value="approvazione-proposta">
                    <input type="hidden" id="article-id" name="articleID" value="<?php print($row["id"]) ?>">
                    
                    <label for="value">Sono </label>
                    <select id="value" name="value" value=1 <?php if (!$row["approval_not_done"]) print("disabled") ?>>
                        <option value=-2>Fortemente contrario</option>
                        <option value=-1>Contrario</option>
                        <option value=0>Neutrale</option>
                        <option value=1 selected>Favorevole</option>
                        <option value=2>Super favorevole</option>
                    </select> 

                    <button type="submit" <?php if (!$row["approval_not_done"]) print("disabled") ?> >Scelto!</button>
                </form>
            </div>
            <div class="article-details">
                <a href="view-comments.php?articleID=<?php print($row["id"]); ?>">
                    Dettagli | Azioni | Commenti 
                    <span class="attention">
                        <?php if ($row["require_attention"]) print("(!!!)"); ?>
                    </span>
                </a>
            </div>
        </div>
        <!-- <div class="debug-text"><?php print_r($row); ?></div> -->
    <?php endwhile ?>
</div>