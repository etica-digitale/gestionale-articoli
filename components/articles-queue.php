<?php if (!$includeOK) die() ?>
<?php 
        $sql = '
            SELECT 
                a.id,
                (
                    (eupa.id IS NULL AND a.status = "proposto")
                    OR (eusa.id IS NULL AND a.status = "in-lavorazione")
                    OR (a.assignedTO = :user AND a.status = "in-lavorazione" AND es.text IS NULL) 
                ) AS require_attention        
            FROM articles_am a
            LEFT JOIN events_am es ON es.article = a.id AND es.type = "riassunto"
            AND NOT EXISTS(
                SELECT * FROM events_am e2
                WHERE e2.article = es.article AND e2.type = "riassunto" AND e2.createdAt > es.createdAt
            )    
            LEFT JOIN events_am eupa ON eupa.article = a.id AND eupa.user = :user AND eupa.type = "approvazione-proposta"
                AND NOT EXISTS (
                    SELECT * FROM events_am e2
                    WHERE e2.article = eupa.article
                        AND e2.user = eupa.user 
                        AND e2.type = "approvazione-proposta"
                        AND e2.createdAt > eupa.createdAt
                )
            LEFT JOIN events_am eusa ON eusa.article = a.id AND eusa.user = :user AND eusa.type = "approvazione-riassunto"
                AND NOT EXISTS (
                    SELECT * FROM events_am e2
                    WHERE e2.article = eusa.article
                        AND e2.user = eusa.user 
                        AND e2.type = eusa.type
                        AND e2.createdAt > eusa.createdAt
                )
            WHERE status = "proposto" OR status = "approvato" OR status = "in-lavorazione"
            ORDER BY a.createdAt ASC
        ';

        $sth = $db->prepare($sql);
        $sth->execute(array('user' => $_SESSION["id"])); 
        $res = $sth->fetchAll(PDO::FETCH_ASSOC);
?>
<?php if (count($res) > 0): ?>
    <div id="articles-list">
        <h1> Articoli in coda (<?php print(count($res)) ?>)</h1>
        <?php foreach ($res as $row): ?>
            <div class="queued-article">
                <a href="view-comments.php?articleID=<?php print($row["id"]); ?>">
                    #<?php print($row["id"]) ?>
                    <a href="view-comments.php?articleID=<?php print($row["id"]); ?>">
                        <span class="attention">
                            <?php if ($row["require_attention"]) print("(!!!)"); ?>
                        </span>
                    </a>
                </a>
            </div>
        <?php endforeach ?>
    </div>
<?php endif ?>