<svg
   width="640"
   height="110"
   viewBox="0 0 169.33333 29.104167"
   version="1.1"
   id="status-indicator"
   alt="Un indicatore che indica visualmente lo status e il in-lavorazione"
   sodipodi:docname="barra_articoli.svg"
   inkscape:version="1.1.2 (0a00cf5339, 2022-02-04)"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
  <sodipodi:namedview
     id="namedview7"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:document-units="mm"
     showgrid="false"
     inkscape:snap-bbox="true"
     inkscape:zoom="0.74441074"
     inkscape:cx="358.00128"
     inkscape:cy="-55.07712"
     inkscape:window-width="1920"
     inkscape:window-height="935"
     inkscape:window-x="0"
     inkscape:window-y="32"
     inkscape:window-maximized="1"
     inkscape:current-layer="layer1"
     units="px"
     inkscape:snap-path-clip="false" />
  <defs
     id="defs2" />
  <g
     inkscape:label="Capa 1"
     inkscape:groupmode="layer"
     id="layer1">
    <path
       style="fill:#b3b3b3;stroke:#bebebe;stroke-width:4.08461;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       d="m 19.062441,14.464886 c 42.540865,0 85.081729,0 127.622599,0"
       id="path1335"
       inkscape:export-filename="fase1.png"
       inkscape:export-xdpi="166.79054"
       inkscape:export-ydpi="166.79054"
       sodipodi:nodetypes="cc" />
    <path
       id="path910" 
       class="
         indicator 
         <?php if ($statusNumber > 0 && $statusNumber >= 1) print("indicator-enabled") ?>
         <?php if ($status == "proposto") print("indicator-active") ?>
       "
       style="fill-opacity:1;stroke-width:0.873001;stroke-linecap:round"
       sodipodi:type="inkscape:offset"
       inkscape:radius="0"
       inkscape:original="M 53.300781 96.019531 A 7.8422685 7.8422685 0 0 0 45.458984 103.86133 A 7.8422685 7.8422685 0 0 0 53.300781 111.70508 A 7.8422685 7.8422685 0 0 0 61.144531 103.86133 A 7.8422685 7.8422685 0 0 0 53.300781 96.019531 z "
       d="m 53.300781,96.019531 a 7.8422685,7.8422685 0 0 0 -7.841797,7.841799 7.8422685,7.8422685 0 0 0 7.841797,7.84375 7.8422685,7.8422685 0 0 0 7.84375,-7.84375 7.8422685,7.8422685 0 0 0 -7.84375,-7.841799 z"
       inkscape:export-filename="fase1.png"
       inkscape:export-xdpi="166.79054"
       inkscape:export-ydpi="166.79054"
       transform="matrix(1.727106,0,0,1.727106,-77.364837,-164.90167)" />
    <path
       id="path910-5"
       class="
         indicator 
         <?php if ($statusNumber > 0 && $statusNumber >= 2) print("indicator-enabled") ?>
         <?php if ($status == "approvato") print("indicator-active") ?>
       "
       style="fill-opacity:1;stroke-width:0.873001;stroke-linecap:round"
       sodipodi:type="inkscape:offset"
       inkscape:radius="0"
       inkscape:original="M 53.300781 96.019531 A 7.8422685 7.8422685 0 0 0 45.458984 103.86133 A 7.8422685 7.8422685 0 0 0 53.300781 111.70508 A 7.8422685 7.8422685 0 0 0 61.144531 103.86133 A 7.8422685 7.8422685 0 0 0 53.300781 96.019531 z "
       d="m 53.300781,96.019531 a 7.8422685,7.8422685 0 0 0 -7.841797,7.841799 7.8422685,7.8422685 0 0 0 7.841797,7.84375 7.8422685,7.8422685 0 0 0 7.84375,-7.84375 7.8422685,7.8422685 0 0 0 -7.84375,-7.841799 z"
       transform="matrix(1.727106,0,0,1.727106,-30.52448,-164.90167)"
       inkscape:export-filename="fase1.png"
       inkscape:export-xdpi="166.79054"
       inkscape:export-ydpi="166.79054" />
    <path
       id="path910-5-4"
       class="
         indicator 
         <?php if ($statusNumber > 0 && $statusNumber >= 3) print("indicator-enabled") ?>
         <?php if ($status == "in-lavorazione") print("indicator-active") ?>
       "
       style="fill-opacity:1;stroke-width:0.873001;stroke-linecap:round"
       sodipodi:type="inkscape:offset"
       inkscape:radius="0"
       inkscape:original="M 53.300781 96.019531 A 7.8422685 7.8422685 0 0 0 45.458984 103.86133 A 7.8422685 7.8422685 0 0 0 53.300781 111.70508 A 7.8422685 7.8422685 0 0 0 61.144531 103.86133 A 7.8422685 7.8422685 0 0 0 53.300781 96.019531 z "
       transform="matrix(1.727106,0,0,1.727106,16.315878,-164.90167)"
       d="m 53.300781,96.019531 a 7.8422685,7.8422685 0 0 0 -7.841797,7.841799 7.8422685,7.8422685 0 0 0 7.841797,7.84375 7.8422685,7.8422685 0 0 0 7.84375,-7.84375 7.8422685,7.8422685 0 0 0 -7.84375,-7.841799 z"
       inkscape:export-filename="fase1.png"
       inkscape:export-xdpi="166.79054"
       inkscape:export-ydpi="166.79054" />
    <path
       id="path910-5-4-9"
       class="
         indicator 
         <?php if ($statusNumber > 0 && $statusNumber >= 4) print("indicator-enabled") ?>
       "
       style="fill-opacity:1;stroke-width:0.873001;stroke-linecap:round"
       sodipodi:type="inkscape:offset"
       inkscape:radius="0"
       inkscape:original="M 53.300781 96.019531 A 7.8422685 7.8422685 0 0 0 45.458984 103.86133 A 7.8422685 7.8422685 0 0 0 53.300781 111.70508 A 7.8422685 7.8422685 0 0 0 61.144531 103.86133 A 7.8422685 7.8422685 0 0 0 53.300781 96.019531 z "
       transform="matrix(1.727106,0,0,1.727106,63.156235,-164.90167)"
       d="m 53.300781,96.019531 a 7.8422685,7.8422685 0 0 0 -7.841797,7.841799 7.8422685,7.8422685 0 0 0 7.841797,7.84375 7.8422685,7.8422685 0 0 0 7.84375,-7.84375 7.8422685,7.8422685 0 0 0 -7.84375,-7.841799 z"
       inkscape:export-filename="fase1.png"
       inkscape:export-xdpi="166.79054"
       inkscape:export-ydpi="166.79054" />
  </g>
</svg>
