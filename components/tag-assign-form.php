<?php if (!$includeOK) die() ?>
<div>
    <h2>Assegnamento tag</h2>
    <form method="POST">
        <input type="hidden" id="type" name="type" value="assegnamento-tag">

        <label for="values">Valore:</label>
        <select id="values" name="values[]" multiple>
            <?php
                $sql = '
                    SELECT 
                        t.id,
                        t.text,
                        (at.article IS NOT NULL) AS present 
                    FROM available_article_tags_am t
                    LEFT JOIN article_tags_am at ON at.tag = t.id AND at.article = ?;
                ';
                $sth = $db->prepare($sql);
                $sth->execute(array($_GET['articleID']));
            ?>
            <?php while ($row = $sth->fetch(PDO::FETCH_ASSOC)): ?>
                <option <?php if($row["present"]) print("selected"); ?> value="<?php print($row["id"]); ?>"><?php print($row["text"]); ?></option>
            <?php endwhile ?>
        </select> 

        <button type="submit">Applica!</button>
    </form>
</div>