<?php if (!$includeOK) die() ?>
<div id="menu">
    <a class="menu-item" id="menu-homepage" href="index.php">        
        Lista articoli
    </a>
    <a class="menu-item" id="menu-propose" href="propose-article.php">        
        Proponi un nuovo articolo
    </a>
    <div class="menu-item" id="menu-username">
        👤<?php print($_SESSION["username"]) ?>
    </div>    
</div>
