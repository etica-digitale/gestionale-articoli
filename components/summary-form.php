<?php if (!$includeOK) die() ?>
<?php if ($status == "in-lavorazione" || $status == "pubblicato"):?>
    <div>
        <h2>Riassunto</h2>
        <form method="POST">
            <input type="hidden" id="type" name="type" value="riassunto">
            <label for="comment"><b>Riassunto</b></label>
            <textarea name="comment"><?php print($lastSummary); ?></textarea>
            <button type="submit">Fatto!</button>
        </form>
    </div>
<?php endif ?>