<?php if (!$includeOK) die() ?>
<?php
    $sql = '
        SELECT 
            (eupa.id IS NULL AND a.status = "proposto") AS approve_proposal,
            (eusa.id IS NULL AND a.status = "in-lavorazione") AS approve_summary,
            (a.assignedTO = :user AND a.status = "in-lavorazione" AND es.id IS NULL) AS write_summary
        FROM articles_am a
        LEFT JOIN events_am es ON es.article = a.id AND es.type = "riassunto"
            AND NOT EXISTS(
                SELECT * FROM events_am e2
                WHERE e2.article = es.article AND e2.type = "riassunto" AND e2.createdAt > es.createdAt
            )
        LEFT JOIN events_am eupa ON eupa.article = a.id AND eupa.user = :user AND eupa.type = "approvazione-proposta"
            AND NOT EXISTS (
                SELECT * FROM events_am e2
                WHERE e2.article = eupa.article
                    AND e2.user = eupa.user 
                    AND e2.type = "approvazione-proposta"
                    AND e2.createdAt > eupa.createdAt
            )
        LEFT JOIN events_am eusa ON eusa.article = a.id AND eusa.user = :user AND eusa.type = "approvazione-riassunto"
            AND NOT EXISTS (
                SELECT * FROM events_am e2
                WHERE e2.article = eusa.article
                    AND e2.user = eusa.user 
                    AND e2.type = eusa.type
                    AND e2.createdAt > eusa.createdAt
            )
        WHERE a.id = :articleID
    ';
    $sth = $db->prepare($sql);
    $sth->execute(array('user' => $_SESSION["id"], 'articleID' => $_GET['articleID']));
    $row = $sth->fetch(PDO::FETCH_ASSOC);        
?>
<?php if ($row["approve_proposal"] || $row["write_summary"] || $row["approve_summary"]): ?>
    <div>
        <h1>Cosa c'è da fare</h1>
        <div>
            <ul>
                <?php if ($row["approve_proposal"]): ?>
                    <li>🗳️ Approvare (o rifiutare) la proposta</li>
                <?php endif ?>
                <?php if ($row["write_summary"]): ?>
                    <li>✍️ Scrivere il riassunto</li>
                <?php endif ?>
                <?php if ($row["approve_summary"]): ?>
                    <li>🔍 Leggere e approvare il riassunto</li>
                <?php endif ?>
            </ul>
        </div>
    </div>
<?php endif ?>