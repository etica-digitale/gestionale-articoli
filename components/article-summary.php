<?php if (!$includeOK) die() ?>
<?php
    $sql = '
        SELECT 
            e.text 
        FROM events_am e
        WHERE e.article = ? AND e.type = "riassunto" 
        AND NOT EXISTS(
            SELECT * FROM events_am e2
            WHERE 
                e2.article = e.article AND 
                e2.type = e.type AND 
                e2.createdAt > e.createdAt
        )';
    $sth = $db->prepare($sql);
    $sth->execute(array($_GET['articleID']));
    $row = $sth->fetch(PDO::FETCH_ASSOC);
            
    $lastSummary = "";
    if ($row) 
        $lastSummary = $row["text"];
?>
<?php if ($lastSummary != ""): ?>
    <div>
        <h1>RIASSUNTO</h1>
        <p><?php print(nl2br($lastSummary)); ?></p>
    </div>
<?php endif ?>