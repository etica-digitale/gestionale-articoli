<?php if (!$includeOK) die() ?>
<div>
    <div id="article-info">
        <div id="article-info-id"><strong>ID: </strong> <?php print($article["id"]); ?></div>
        <div id="article-info-status"><strong>Stato: </strong> <?php print($status); ?></div>
        <div id="article-info-link"><a href="<?php print($article["link"]) ?>"><?php print($article["link"]); ?></a></div>
        <div id="article-info-proposing-user"><strong>Proposto da: </strong> <?php print($article["proposing_user"]); ?></div>
        <div id="article-info-created-at"><strong>Proposto il: </strong> <?php print($article["createdAt"]); ?></div>
        <div id="article-info-expire-date"><strong>Scade il: </strong> <?php print($article["expireDate"]); ?></div>
        <?php if (is_null($article["assigned_username"])): ?>
            <div id="article-info-assigned-to"><strong>Non assegnato</strong></div>
        <?php else: ?>
            <div id="article-info-assigned-to"><strong>Assegnato a: </strong> <?php print($article["assigned_username"]); ?></div>
        <?php endif ?>
        <div id="article-info-approval-info">
            <strong>Approvazioni proposta</strong>
            punteggio=<?php print($proposalStatus["avg"]) ?>,
            quorum=<?php print($proposalStatus["count"] . "/" . $usersCount); ?>
        </div>
        <div id="article-info-tag-list">
            <?php
                $sql = '
                    SELECT 
                        at.text
                    FROM article_tags_am t
                    INNER JOIN available_article_tags_am at ON at.id = t.tag 
                    WHERE t.article = ?;
                ';
                $sth = $db->prepare($sql);
                $sth->execute(array($_GET['articleID']));
            ?>
            <strong>TAG: </strong>
            <?php while ($row = $sth->fetch(PDO::FETCH_ASSOC)): ?>
                #<?php print($row["text"]); ?> 
            <?php endwhile ?>
        </div>
    </div>
</div>