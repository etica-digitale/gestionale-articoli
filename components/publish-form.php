<?php if (!$includeOK) die() ?>
<?php if ($summaryStatus >= $threshold_approved_summary): ?>
    <div>
        <h2>Pubblica!</h2>
        <p>(attualmente non è automatica la pubblicazione su telegram e social)</p>
        <form method="POST">
            <input type="hidden" id="type" name="type" value="pubblicazione">
            <button type="submit">Pubblica!</button>
        </form>
    </div>
<?php endif ?>