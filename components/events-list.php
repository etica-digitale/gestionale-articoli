<?php if (!$includeOK) die() ?>
<div>
    <h1>Linea temporale</h1>
    <div>
        <?php
            $sql = '
                SELECT 
                    e.*, 
                    u.username AS username
                FROM events_am e
                INNER JOIN users_am u ON u.id = e.user
                WHERE e.article = ?';
            $sth = $db->prepare($sql);
            $sth->execute(array($_GET['articleID']));
        ?>
        <?php while ($row = $sth->fetch(PDO::FETCH_ASSOC)): ?>
            <div class="event">
                <div class="event-header">
                    <div class="event-user">[<?php print($row["username"]) ?>]</div>    
                    <div class="event-type"><?php print($row["type"]) ?></div>
                    <div class="event-createdAt "><?php print($row["createdAt"]) ?></div>    
                </div>
                <div class="event-text"><?php print(nl2br($row["text"])) ?></div>
                <!-- <?php if ($row["value"]): ?>
                    <div><strong>Valore:</strong> <?php print($row["value"]) ?></div>
                <?php endif ?> -->
            </div>
        <?php endwhile ?>
    </div>
</div>