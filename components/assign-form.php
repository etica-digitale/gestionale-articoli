<?php if (!$includeOK) die() ?>
<?php if ($status == "approvato" || $status == "in-lavorazione"): ?>
    <div>
        <h2>Assegnazione</h2>
        <form method="POST">
            <input type="hidden" id="type" name="type" value="assegnazione">

            <label for="user">Utente:</label>
            <select id="users" name="user">
                <?php 
                    $sql = '
                        SELECT 
                            u.id, 
                            u.username
                        FROM users_am u WHERE id <> ' . $system_user_id; 
                ?>
                <?php foreach ($db->query($sql, PDO::FETCH_ASSOC) as $row): ?>
                    <option name="user" value="<?php print($row["id"]); ?>"><?php print($row["username"]); ?></option>
                <?php endforeach ?>
            </select> 

            <button type="submit">Fatto!</button>
        </form>
    </div>
<?php endif ?>
