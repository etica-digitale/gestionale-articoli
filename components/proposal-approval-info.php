<?php if (!$includeOK) die() ?>
<div>
    <h1>
        Approvazioni proposta 
    </h1>
    <div>
        <?php 
            $sql = "
                SELECT 
                    u.username,
                    e.value
                FROM users_am u
                LEFT JOIN events_am e 
                    ON e.article = ? 
                    AND e.user = u.id 
                    AND e.type = 'approvazione-proposta'
                    AND NOT EXISTS (
                        SELECT * FROM events_am e2
                        WHERE e2.article = e.article
                            AND e2.user = u.id 
                            AND e2.type = e.type
                            AND e2.createdAt > e.createdAt
                    )
                WHERE 
                    u.id <> ?";
            $sth = $db->prepare($sql);
            $res = $sth->execute(array($_GET['articleID'], $system_user_id));
        ?>
        <?php while ($row = $sth->fetch(PDO::FETCH_ASSOC)): ?>
            <div class="approval approval-<?php print($row["value"])?>">
                <?php print($row["username"]) ?>
            </div>
        <?php endwhile ?>
    </ul>
</div>