-- SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

CREATE DATABASE articles_manager;

CREATE TABLE users_am (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    username VARCHAR(128) NOT NULL,
    password VARCHAR(512) NOT NULL /*SHA2(256)*/
);

CREATE TABLE articles_am (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    link VARCHAR(4096) NOT NULL,
    createdAt TIMESTAMP NOT NULL, 
    expireDate TIMESTAMP NOT NULL,
    status VARCHAR(32) NOT NULL DEFAULT "sconosciuto",
    assignedTO int REFERENCES users_am(id)
);

CREATE TABLE events_am (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    createdAt TIMESTAMP NOT NULL,
    user int NOT NULL REFERENCES users_am(id),
    type VARCHAR(32) NOT NULL,
    text TEXT NOT NULL,
    value SMALLINT,
    article int NOT NULL references articles_am(id)
);

CREATE TABLE available_article_tags_am (
    id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    text varchar(128) NOT NULL,
    createdAt TIMESTAMP NOT NULL
);

CREATE TABLE article_tags_am (
    article int NOT NULL references articles_am(id),
    tag int NOT NULL references available_article_tags_am(id),
    createdAt TIMESTAMP NOT NULL,
    PRIMARY KEY (article, tag)
);

