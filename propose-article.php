<?php
    // SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
    //
    // SPDX-License-Identifier: AGPL-3.0-or-later
    session_start();
    require_once("config.php");
    require_once("utils.php");
    checkLoginOrGoToLoginPage();

    if(isset($_POST['link']) && isset($_POST['comment'])) {
        proposeArticle($db, $expireTimeoutDays, $_SESSION["id"], $_POST['link'], $_POST['comment']);
        header("location:index.php");
        exit(0);
    }
?>  
<html>
    <?php include("components/common-head.php") ?>
    <body>
        <?php include("components/menu.php"); ?>
        <div>
            <form id="propose-form" method="post">
                <label id="propose-form-link-label" for="link"><strong>Link</strong> (della notizia)</label>
                <input id="propose-form-link" type="text" name="link" required>
                <label id="propose-form-comment-label" for="comment"><strong>Commento</strong></label>
                <textarea id="propose-form-comment" name="comment" 
                    placeholder="Una descrizione breve della notizia, un piccolo riassunto, il titolo e il sottotitolo della notizia, un estratto del testo, insomma qualunque informazione che aiuta brevemente a capire di cosa parla la notizia senza avere la necessità di leggerlo a fini dell'approvazione"
                >
                </textarea>
                <button id="propose-form-propose" type="submit">Proponi</button>
            </form>
        </div>
    </body>
</html>
