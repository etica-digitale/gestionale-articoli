<?php
    // SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
    //
    // SPDX-License-Identifier: AGPL-3.0-or-later

    session_start();
    require_once("config.php");
    include_once("utils.php");
    checkLoginOrGoToLoginPage();

    $usersCount = getUsersCount($db);

    if (isset($_POST["action"]) && isset($_POST["articleID"])) {
        switch ($_POST["action"]) {
            case 'approvazione-proposta':
                createEvent($db, $_SESSION["id"], $_POST['articleID'], 'approvazione-proposta', '', '');
                break;
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit(0);
    }
?>
<html>
    <?php include("components/common-head.php") ?>
    <body>
        <?php include("components/menu.php"); ?>
        <?php include("components/articles-queue.php"); ?>
        <?php include("components/articles-list.php"); ?>
    </body>
</html>
