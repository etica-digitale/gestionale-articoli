<?php
    // SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
    //
    // SPDX-License-Identifier: AGPL-3.0-or-later

    session_start();
    require_once("config.php");
    require_once("utils.php");
    
    $invalidCredentials = false;

    if(isset($_POST['username']) && isset($_POST['password'])) {
        $sth = $db->prepare('SELECT * FROM users_am WHERE username=? AND password=sha2(?, 256)');
        $sth->execute(array($_POST['username'], $_POST['password']));
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            $_SESSION["username"] = $result["username"];
            $_SESSION["id"] = $result["id"];
            header("location:index.php");
        } else {
            $invalidCredentials = true;
        }
    }
?>
<html>
    <?php include("components/common-head.php") ?>
    <body>
        <?php if ($invalidCredentials) { ?>
            <p>Credenziali invalide!</p>
        <?php }?>
        <h1>Login</h1>
        <form method="post">
            <label for="username"><b>Username: </b></label>
            <input type="text" name="username" required>
            <label for="password"><b>Password: </b></label>
            <input type="password" name="password" required>
            <button type="submit">Login</button>
        </form>
    </body>
</html>
