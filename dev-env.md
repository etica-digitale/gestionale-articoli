<!--
SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>

SPDX-License-Identifier: CC0-1.0
-->

docker run -P --rm --env MARIADB_USER=example-user --env MARIADB_PASSWORD=my_cool_secret --env MARIADB_ROOT_PASSWORD=my-secret-pw  mariadb:latest

docker run -d -p 8082:80 --mount type=bind,source="$(pwd)",target=/var/www/html php:apache