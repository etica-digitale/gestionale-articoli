<?php
    // SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
    //
    // SPDX-License-Identifier: AGPL-3.0-or-later
    require_once("config.php");
    require_once("utils.php");

    if (!isset($_GET['key']) || $_GET['key'] != $key) {
        exit(0);
    }
    print("<?xml version='1.0' encoding='UTF-8'?>");
?>  
<rss version='2.0'>
    <channel>

    <title>Feed del gestionale</title>
    <link>https://gestionale-articoli.eticadigitale.org</link>
    <description>Feed del gestionale</description>

    <?php 
        $sql = '
            SELECT
                 e.*,
                 a.link, 
                 u.username
            FROM events_am e 
            INNER JOIN articles_am a ON a.id = e.article
            INNER JOIN users_am u ON u.id = e.user 
            ORDER BY e.createdAt DESC 
            LIMIT 10'; 
    ?>
    <?php foreach ($db->query($sql, PDO::FETCH_ASSOC) as $row): ?>
    <item>
        <title>Evento #<?php print($row["id"]) ?> - <?php print($row["type"]); ?> (<?php print($row["username"]) ?>)</title>
        <link>https://gestionale-articoli.eticadigitale.org/view-comments.php?articleID=<?php print($row["id"]); ?></link>
        <description>
            <?php print($row["link"]); ?> <br />
            <?php print(htmlspecialchars(nl2br($row["text"]))); ?>

        </description>
    </item>    
    <?php endforeach ?>
    </channel>
</rss>