<?php
    // SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
    //
    // SPDX-License-Identifier: AGPL-3.0-or-later

    session_start();
    require_once("config.php");
    require_once("utils.php");
    checkLoginOrGoToLoginPage();

    $article = getArticleInfo($db, $_GET['articleID']);
    $proposalStatus = getArticleProposalApprovalInfo($db, $_GET['articleID']);
    $summaryStatus = getArticleSummartApprovalInfoCount($db, $_GET['articleID']);
    $status = $article["status"];
    $statusNumber = status2int($status);
    $usersCount = getUsersCount($db);

    if(isset($_POST['type']))
    {
        $checkAutomation = false;
        switch ($_POST['type']) {
            case 'approvazione-proposta':
            case 'approvazione-riassunto':
                createEvent($db, $_SESSION["id"], $_GET['articleID'], $_POST['type'], $_POST['comment'], $_POST["value"]);
                $checkAutomation = true;
                break;
            case 'commento':
            case 'riassunto':
                createEvent($db, $_SESSION["id"], $_GET['articleID'], $_POST['type'], $_POST['comment'], null);
                break;
            case 'assegnazione':
                $username = getUsernameByID($db, $_POST["user"]);
                createEvent($db, $_SESSION["id"], $_GET['articleID'], 'assegnazione', 'Assegnato a ' . $username, null);
                assignUserToArticle($db, $_GET['articleID'], $_POST["user"]);
                $checkAutomation = true;
                break;
            case 'cambio-stato':
                createEvent($db, $_SESSION["id"], $_GET['articleID'], 'cambio-stato', 'cambiato stato a ' . $_POST["state"], null);
                setArticleStatus($db, $_GET['articleID'], $_POST["state"]);
                break;
            case 'pubblicazione':
                createEvent($db, $_SESSION["id"], $_GET['articleID'], 'pubblicazione', 'PUBBLICATO!', null);
                setArticleStatus($db, $_GET['articleID'], 'pubblicato');
                break;
            case 'assegnamento-tag':
                $availabeTags = getAvailableTags($db);

                foreach($availabeTags as $tag) {
                    if (in_array($tag["id"], $_POST["values"])) {
                        assignTagToArticle($db, $_GET['articleID'], $tag["id"]);
                    } else {    
                        removeAssignTagToArticle($db, $_GET['articleID'], $tag["id"]);
                    }
                }

                break;
        }

        if($checkAutomation) {    
            $proposalStatus = getArticleProposalApprovalInfo($db, $_GET['articleID']);
            
            if ($status == "proposto" && $proposalStatus["count"] >= $quorum_approval_proposal) {
                if ($proposalStatus["avg"] >= $threshold_approved_proposal) {
                    createEvent($db, $system_user_id, $_GET['articleID'], 'cambio-stato', 'cambiato stato a approvato per raggiunta soglia superiore e quorum', null);
                    setArticleStatus($db, $_GET['articleID'], 'approvato');
                } elseif ($proposalStatus["avg"] <= $threshold_discarded_proposal) {
                    createEvent($db, $system_user_id, $_GET['articleID'], 'cambio-stato', 'cambiato stato a scartato per raggiunta soglia inferiore e quorum', null);
                    setArticleStatus($db, $_GET['articleID'], 'scartato');
                }
            }

            if ($status == "approvato" && $_POST["type"] == "assegnazione") {
                createEvent($db, $system_user_id, $_GET['articleID'], 'cambio-stato', 'cambiato stato a in-lavorazione in quanto l\'articolo è stato assegnato', null);
                setArticleStatus($db, $_GET['articleID'], 'in-lavorazione');
            }
        }
    
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit(0);
    }
?>  
<html>
    <?php include("components/common-head.php") ?>
    <body>
        <?php include("components/menu.php"); ?>
        <?php include("components/article-info.php") ?>
        <div id="status-indicator-container">
            <?php include("components/status-indicator.svg.php") ?>
        </div>
        <?php include("components/article-summary.php") ?>
        <?php include("components/article-what-to-do.php") ?>
        <div>
            <h1>Azioni sull'articolo</h1>
            <?php include("components/proposal-approval-form.php") ?>
            <?php include("components/comment-form.html") ?>
            <?php include("components/assign-form.php") ?>
            <?php include("components/tag-assign-form.php") ?>
            <?php include("components/assign-form.php") ?>
            <?php if ($status == "in-lavorazione"): ?>
                <?php include("components/summary-approval-form.html") ?>
                <?php include("components/publish-form.php") ?>
            <?php endif ?>
        </div>
        <?php include("components/proposal-approval-info.php") ?>
        <?php include("components/summary-approval-info.php") ?>
        <?php include("components/events-list.php") ?>
        <?php include("components/change-state-form.html") ?>
    </body>
</html>
