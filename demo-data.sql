-- SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
--
-- SPDX-License-Identifier: CC0-1.0

INSERT INTO users_am(username, password) VALUES ('amreo', sha2('demo', 256));
INSERT INTO users_am(username, password) VALUES ('rb', sha2('demo', 256));
INSERT INTO users_am(username, password) VALUES ('sistema', '-');

INSERT INTO available_article_tags_am(text) VALUES ('notizia');
INSERT INTO available_article_tags_am(text) VALUES ('documento');
INSERT INTO available_article_tags_am(text) VALUES ('guida');
INSERT INTO available_article_tags_am(text) VALUES ('software');
INSERT INTO available_article_tags_am(text) VALUES ('riflessione');
INSERT INTO available_article_tags_am(text) VALUES ('eticadigitale');
INSERT INTO available_article_tags_am(text) VALUES ('AI');
INSERT INTO available_article_tags_am(text) VALUES ('censura');
INSERT INTO available_article_tags_am(text) VALUES ('comunicazione');
INSERT INTO available_article_tags_am(text) VALUES ('leak');
INSERT INTO available_article_tags_am(text) VALUES ('legge');
INSERT INTO available_article_tags_am(text) VALUES ('privacy');
INSERT INTO available_article_tags_am(text) VALUES ('psiche');
INSERT INTO available_article_tags_am(text) VALUES ('socialnetwork');
INSERT INTO available_article_tags_am(text) VALUES ('sorveglianza');
INSERT INTO available_article_tags_am(text) VALUES ('sicurezza');
INSERT INTO available_article_tags_am(text) VALUES ('softwarelibero');

