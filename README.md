<!--
SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>

SPDX-License-Identifier: CC0-1.0
-->

# gestionale-articoli

Gestionale per la gestione dei riassunti degli articoli

# L'installazione
Il gestionale si può installare semplicemente copiando i file nella DocumentRoot di apache (solitamente /var/www/html), rinonimando config-example.php in config.php e sistemando i valori,
creando un database mysql e eseguendogli le query in schema.sql

# Sviluppo
Ogni volta che fate le modifiche controllate con `reuse lint`
