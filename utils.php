<?php
    // SPDX-FileCopyrightText: 2022 Andrea Laisa (amreo) <amreo@linux.it>
    //
    // SPDX-License-Identifier: AGPL-3.0-or-later

    function checkLoginOrGoToLoginPage() {
        if(!isset($_SESSION["username"]) && !isset($_SESSION["id"])) {
            header("location:login.php");
            exit(0);
        }

        return true;
    }

    function getUsersCount($db) {
        $sql = "SELECT (count(*)-1) AS count FROM users_am";
        return $db->query($sql, PDO::FETCH_ASSOC)->fetch()["count"];
    }

    function createArticle($db, $expireTimeoutDays, $link) {
        $sth = $db->prepare("INSERT INTO articles_am(link, status, expireDate) VALUES (?, 'proposto', date_add(current_timestamp(), INTERVAL ? day))");
        $sth->execute(array($link, $expireTimeoutDays));
        return $db->lastInsertId();
    }

    function createEvent($db, $user, $article, $type, $text, $value) {
        $sth = $db->prepare("INSERT INTO events_am(user, article, type, text, value) VALUES (?, ?, ?, ?, ?)");
        $sth->execute(array($user, $article, $type, $text, $value));
        return $db->lastInsertId();
    }

    function genNewProposalText($link, $comment) {
        return sprintf("%s.\n%s", $link, $comment);
    }

    function proposeArticle($db, $expireTimeoutDays, $user, $link, $comment) {
        $articleID = createArticle($db, $expireTimeoutDays, $link);
        createEvent(
            $db, 
            $user, 
            $articleID, 
            "proposta", 
            genNewProposalText($link, $comment), 
            null
        );
        return $articleID;
    }

    function getArticleInfo($db, $articleID) {
        $sql = '
            SELECT 
                a.*, 
                pu.username AS proposing_user,
                au.username AS assigned_username
            FROM articles_am a
            INNER JOIN events_am pe ON pe.article = a.id AND pe.type = "proposta"
            INNER JOIN users_am pu ON pu.id = pe.user
            LEFT JOIN users_am au ON au.id = a.assignedTO
            WHERE a.id = ?
        ';
        $sth = $db->prepare($sql);
        $sth->execute(array($_GET['articleID']));
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    function getArticleProposalApprovalInfo($db, $articleID) {
        $sql = "
            SELECT 
                avg(e.value) AS avg,
                count(e.value) AS count
            FROM events_am e 
            WHERE e.article = ? 
                AND e.type = 'approvazione-proposta' 
                AND NOT EXISTS (
                    SELECT * FROM events_am e2
                    WHERE e2.article = e.article
                        AND e2.user = e.user 
                        AND e2.type = e.type 
                        AND e2.createdAt > e.createdAt
                )
        ";
        $sth = $db->prepare($sql);
        $sth->execute(array($articleID));
        $proposalStatus = $sth->fetch(PDO::FETCH_ASSOC);
        if ($proposalStatus["count"] == 0) {
            $proposalStatus["count"] = 0;
            $proposalStatus["avg"] = 0;
        }
        return $proposalStatus;
    }

    function getArticleSummartApprovalInfoCount($db, $articleID) {
        $sql = "
            SELECT 
                count(e.value) AS count
            FROM events_am e 
            WHERE e.article = ? 
                AND e.type = 'approvazione-riassunto' 
                AND NOT EXISTS (
                    SELECT * FROM events_am e2
                    WHERE e2.article = e.article
                        AND e2.user = e.user 
                        AND e2.type = e.type 
                        AND e2.createdAt > e.createdAt
                )
        ";
        $sth = $db->prepare($sql);
        $sth->execute(array($articleID));
        return $sth->fetch(PDO::FETCH_ASSOC)["count"]; 
    }

    function getUsernameByID($db, $username) {
        $sth = $db->prepare("SELECT username FROM users_am WHERE id = ?");
        $sth->execute(array($_POST["user"]));
        return $sth->fetch(pdo::FETCH_ASSOC)["username"];
    }

    function assignUserToArticle($db, $article, $user) {
        $sth = $db->prepare("UPDATE articles_am SET assignedTO = ? WHERE id=?");
        $sth->execute(array($user, $article));
    }

    function setArticleStatus($db, $article, $newState) {
        $sth = $db->prepare("UPDATE articles_am SET status = ? WHERE id=?");
        $sth->execute(array($newState, $article));
    }

    function status2int($status) {
        switch ($status) {
            case 'proposto':
                return 1;
            case 'approvato':
                return 2;
            case 'in-lavorazione':
                return 3;
            case 'pubblicato':
                return 4;
            case 'scartato':
                return -1;
            default:
                die("stato invalido");
        }
    }

    function assignTagToArticle($db, $article, $tag) {
        $sth = $db->prepare("INSERT INTO article_tags_am(article, tag) VALUES (?, ?) ON DUPLICATE KEY UPDATE article=article");
        $sth->execute(array($article, $tag));
    }

    function removeAssignTagToArticle($db, $article, $tag) {
        $sth = $db->prepare("DELETE FROM article_tags_am WHERE article=? AND tag=?");
        $sth->execute(array($article, $tag));
    }

    function getAvailableTags($db) {
        $sql = "SELECT id, text FROM available_article_tags_am";
        return $db->query($sql, PDO::FETCH_ASSOC)->fetchAll();
    }
?>